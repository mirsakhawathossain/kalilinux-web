FROM kalilinux/kalirolling:latest

RUN apt update && apt install -y kali-linux-default

RUN apt install kali-tools-web

RUN apt install python3

ENTRYPOINT ["/bin/bash"]